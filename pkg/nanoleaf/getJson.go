package nanoleaf

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

var Client RLHTTPClient

//RLHTTPClient Rate Limited HTTP Client
type RLHTTPClient struct {
	host        string
	authToken   string
	client      *http.Client
	Ratelimiter *rate.Limiter
}

//Do dispatches the HTTP request to the network
func (c *RLHTTPClient) Do(req *http.Request) (*http.Response, error) {
	// Comment out the below 5 lines to turn off ratelimiting
	ctx := context.Background()
	err := c.Ratelimiter.Wait(ctx) // This is a blocking call. Honors the rate limit
	if err != nil {
		return nil, err
	}
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//NewClient return http client with a ratelimiter
func NewClient(rl *rate.Limiter, host string, authToken string) error {
	// Create HTTP Client
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	Client = RLHTTPClient{
		host:        host,
		authToken:   authToken,
		client:      client,
		Ratelimiter: rl,
	}
	return nil
}

// Convert JSON to Object
func getJSON(target interface{}) error {
	log.Debug("Preparing Request")
	url := fmt.Sprintf("%s/%s/", Client.host, Client.authToken)
	log.Debug(url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error(err)
	}

	log.Debug("Executing the Request")
	r, err := Client.Do(req)
	log.Debug(r)
	if err != nil {
		log.Error(err)
		return err
	} else {
		log.Debug("Successfully executed the Request")
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
